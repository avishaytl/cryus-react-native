import { StatusBar } from 'expo-status-bar'
import React from 'react'
import SwitchTheme from './src/components/SwitchTheme'
import ThemeManager from './src/themes'
import { Container, Title } from './src/themes/style'
import Constants  from 'expo-constants'
import { Dimensions} from 'react-native'
import Main from './src/components/Main'
import { StoreProvider } from './src/stores'
const {height} = Dimensions.get('window')

function App() {
  return (
    <StoreProvider>
      <ThemeManager>
          <Container minHeight={height} paddingTop={Constants.statusBarHeight}>
            <StatusBar style="auto" />
            <SwitchTheme/>
            <Title>{`Get Stack Overflow posts`}</Title>
            <Main/>
          </Container>  
      </ThemeManager>
    </StoreProvider>
  )
}

export default App