import React from "react"
import { Switch, View } from "react-native"
import styled from "styled-components/native"
import { useTheme } from "../themes"
import { Text } from '../themes/style'

const SwitchView = styled.View`
  align-items: flex-start;
`

const SwitchTheme = () =>{
    const theme = useTheme()
    return(
      <SwitchView>
        <Switch
          value={theme.mode === 'dark'}
          onValueChange={value => theme.setMode(value ? 'dark' : 'light')}
        />
        <Text>
          {`${theme.mode} mode`}
        </Text>
      </SwitchView>
    )
}

export default SwitchTheme