import { observer } from "mobx-react-lite"
import React from "react"
import { useStore } from "../../stores"
import { FooterBarView, Title } from "./style"

const FooterBar = () =>{
    const store = useStore()
    const ObsFooterBar = observer(()=>{
        return(
            <FooterBarView>
                <Title>Total of {store.posts.length} questions found</Title>
            </FooterBarView>
        )
    }) 
    return <ObsFooterBar/>
} 

export default FooterBar