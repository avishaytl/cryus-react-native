import { observer } from "mobx-react"
import React, { useCallback } from "react"
import { FlatList, TouchableWithoutFeedback } from "react-native"
import { useStore } from "../../stores"
import { useTheme } from "../../themes"
import FooterBar from "./FooterBar"
import NoData from "../NoData"
import { Container, ListFooter, SearchListItem, Subtitle, Title } from "./style"
import LoadingIndicator from "../LoadingIndicator"

const ResSearchList = () =>{
  const store = useStore()
    const item = useCallback(({item})=>{
      const onPress = () =>{
        store.actionCurrentPostModalOpen()
        store.setCurrentPost(item)
      }
      return(
        <TouchableWithoutFeedback key={item['question_id']} onPress={onPress}>
          <SearchListItem>
            <Title numberOfLines={1}>{item['title']}</Title>
            <Subtitle numberOfLines={2}>{item['link']}</Subtitle>
          </SearchListItem> 
        </TouchableWithoutFeedback>
      )
    },[])
    const getItemLayout = useCallback((data, index) => ({length: 75, offset: 75 * index, index}),[])
    const keyExtractor = useCallback((item)=>item['question_id'],[])
    const ObsResSearchList = observer(()=>{
      return(
        <Container>
          {store.isRequestRunning && <LoadingIndicator/>}
          {!store.posts.length && !store.isRequestRunning && <NoData/>}
          <FlatList 
            showsVerticalScrollIndicator={false}
            getItemLayout={getItemLayout} 
            keyExtractor={keyExtractor} 
            ListFooterComponent={ListFooter}
            data={store.posts}
            renderItem={item}/>
          <FooterBar/>
        </Container>
      )
    }) 
    return <ObsResSearchList/>
}

export default ResSearchList