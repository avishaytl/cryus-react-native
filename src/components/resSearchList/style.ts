import styled from "styled-components/native"
import { light } from "../../themes/style"
export const Container = styled.View`
    align-items: center;
    justify-content: center;
    flex: 1;
    min-width: 100%;
`
export const SearchListItem = styled.View`
    height: 75px;
    margin-top: 5px;
    min-width: 100%;
    padding: 10px;
`
export const Title = styled.Text` 
    color: ${(p: typeof light) => p.theme.text};
    font-weight: bold;
`
export const Subtitle = styled.Text` 
    color: ${(p: typeof light) => p.theme.text};
    opacity: 0.8;
`

export const ListFooter = styled.View`
    height: 50px;
    min-width: 100%;
`
export const FooterBarView = styled.View`
    position: absolute;
    height: 50px;
    min-width: 100%;
    align-items: center;
    justify-content: center;
    bottom: 0;
    background: #ffffff5f;
`
