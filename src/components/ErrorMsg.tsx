import { observer } from "mobx-react"
import React from "react"
import styled from "styled-components/native"
import { useStore } from "../stores"

const Text = styled.Text`
  text-align: center;
  min-width: 100%;
  color: red;
`

const ErrorMsg = () =>{
    const store = useStore()
    const ObsErrorMsg = observer(()=> <Text>{`${store.responseMsg}`}</Text>)
    return <ObsErrorMsg/>
}

export default ErrorMsg