import React from "react"
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { useTheme } from "../themes"
import { NoDataView } from "../themes/style"

const NoData = () =>{
    const theme = useTheme()
    return(
        <NoDataView>
            <MaterialCommunityIcons name="book-search-outline" size={68} color={theme.mode === 'light' ? '#000' : '#fff'} />
        </NoDataView>
    )
}
export default NoData