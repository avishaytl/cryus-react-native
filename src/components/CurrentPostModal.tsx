import { observer } from "mobx-react"
import React, { useCallback, useEffect } from "react"
import { Dimensions, Linking, Modal, TouchableWithoutFeedback } from "react-native"
import { useStore } from "../stores"
import { BackCloseModal, CurrentPostModalView, CurrentPostModalViewInside, Title } from "../themes/style"
import { Subtitle } from "./resSearchList/style"
const {height, width} = Dimensions.get('window')

const CurrentPostModal = () =>{
    const store = useStore()
    const onPress = useCallback(()=>{
        Linking.openURL(store.currentPost['link'])
    },[])
    const ObsCurrentPostModal = observer(()=>{
        return(
            <Modal
                animationType={'fade'}
                transparent={true}
                statusBarTranslucent
                visible={store.isCurrentPostModalOpen}
                onRequestClose={store.actionCurrentPostModalOpen}> 
                <CurrentPostModalView>
                    <TouchableWithoutFeedback onPress={store.actionCurrentPostModalOpen}>
                        <BackCloseModal width={width} height={height}/>
                    </TouchableWithoutFeedback>
                    <CurrentPostModalViewInside>
                        <Title>
                            {store.currentPost['title']}
                        </Title>
                        <Subtitle>
                            ⭐{store.currentPost['score']}
                        </Subtitle>
                        <TouchableWithoutFeedback onPress={onPress}> 
                            <Subtitle>
                                {store.currentPost['link']}
                            </Subtitle>
                        </TouchableWithoutFeedback>
                    </CurrentPostModalViewInside>
                </CurrentPostModalView>
            </Modal>
        )
    }) 
    return <ObsCurrentPostModal/>
}

export default CurrentPostModal