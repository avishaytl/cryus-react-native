import React, { useEffect } from "react"
import { useStore } from "../stores"
import CurrentPostModal from "./CurrentPostModal"
import ErrorMsg from "./ErrorMsg"
import ResSearchList from "./resSearchList/ResSearchList"
import UserBoard from "./userBoard/UserBoard"

const Main = () =>{
    return(
      <>
          <ErrorMsg/>
          <UserBoard/>
          <ResSearchList/>
          <CurrentPostModal/>
      </>
    )
}

export default React.memo(Main)