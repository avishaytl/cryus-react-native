import React, { useCallback, useEffect, useState } from "react"
import { TextInput, TouchableWithoutFeedback, StyleSheet } from "react-native"
import { useTheme } from "../../themes"
import { Bar, RemoveTextCircle } from "./style"
import { MaterialIcons } from '@expo/vector-icons'
import { useStore } from "../../stores"

const useChangeText = (deafultValue?: string): [string,(val: string)=>void] =>{
  const [searchValue,setValue] = useState('')
  const onChangeText = (val: string) =>{
    setValue(val)
  }
  useEffect(()=>{
    if(deafultValue)
      setValue(deafultValue)
  },[])
  return [searchValue, onChangeText]
}

const UserSearchIdBar = () =>{
    const [searchValue,onChangeText] = useChangeText()
    const theme = useTheme()
    const store = useStore()
    const onRemoveText = useCallback(()=>{
      onChangeText('')
      store.setResponseMsg('')
      store.clearPost()
    },[])
    const onEndEditing = useCallback((e)=>{
      console.debug('searchValue',e.nativeEvent.text)
      store.setSearchFilter(e.nativeEvent.text)
    },[])
    return(
      <Bar>
        <TextInput 
          selectTextOnFocus
          value={searchValue}
          onChangeText={text => onChangeText(text)} 
          onEndEditing={onEndEditing} 
          style={[styles.input,{backgroundColor: theme.mode === 'light' ? '#fff' : '#cecece'}]} 
          placeholder="user id"/>
        <TouchableWithoutFeedback onPress={onRemoveText}>
          <RemoveTextCircle>
            <MaterialIcons name="cancel" size={22} color={theme.mode === 'light' ? '#000' : '#fff'} />
          </RemoveTextCircle>
        </TouchableWithoutFeedback>
      </Bar>
    )
}

const styles = StyleSheet.create({
  input: {
    minWidth:'80%',
    height:40,
    borderRadius: 5,
    paddingRight: 5,
    textAlign: 'left'
  }
})

export default UserSearchIdBar