import React, { useCallback, useState } from "react"
import {  Bar, QuestionMode, QuestionModeTab, QuestionModeTabLabel } from "./style"
import {TouchableWithoutFeedback} from 'react-native'
import {Text} from '../../themes/style'
import { useStore } from "../../stores"

const UserQuestionMode = () =>{
    const [selected,setSelected] = useState(`Dates`)
    const keys = [`Views`,`Answers`,`Dates`]
    const store = useStore()
    return(
      <Bar>
        <Text>{`Questions:`}</Text>
        <QuestionMode>
          {keys.map((label: string)=> {
            const onPress = useCallback(() =>{
              setSelected(label)
              store.sortPostByField(label)
            },[])
            return (<TouchableWithoutFeedback onPress={onPress} key={label}>
                      <QuestionModeTab isSelected={label === selected && `#98b6e6`}>
                        <QuestionModeTabLabel isSelected={label === selected && `#ececec`}>
                          {label}
                        </QuestionModeTabLabel>
                      </QuestionModeTab>
                    </TouchableWithoutFeedback>)
          })}
        </QuestionMode>
      </Bar>
    )
}

export default UserQuestionMode