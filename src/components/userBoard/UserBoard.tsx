import React from "react"
import { Container} from "./style"
import UserSearchIdBar from "./UserSearchIdBar"
import UserQuestionMode from "./UserQuestionMode"
import UserInfo from "./UserInfo"

const UserBoard = () =>{
    return(
      <Container> 
        <UserSearchIdBar/>
        <UserInfo/>
        <UserQuestionMode/>
      </Container>
    )
}

export default React.memo(UserBoard)