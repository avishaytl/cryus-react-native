import React from "react"
import { useTheme } from "../../themes"
import { FontAwesome } from '@expo/vector-icons'
import { Info, UserImageView, UserLabelView, Label, UserImage} from "./style"
import {Dimensions, ImageBackground, StyleSheet} from 'react-native'
import { observer } from "mobx-react"
import { useStore } from "../../stores"
const {width} = Dimensions.get('window')

const UserInfo = () =>{
  const theme = useTheme()
  const store = useStore()
  const ObsUserInfo = observer(()=>{
    return(
      <Info>
        <UserImageView>
          <UserImage>
            {store.cuurentOwner ? <ImageBackground style={[styles.image,{height: width/4, width: width/4}]} source={{uri: store.cuurentOwner?.profile_image}}/> : <FontAwesome name="user" size={width/4.5} color={theme.mode === 'light' ? '#000' : '#fff'} />}
          </UserImage>
        </UserImageView>
        <UserLabelView>
          <Label>{store.cuurentOwner?.display_name}</Label>
          <Label>{store.cuurentOwner?.reputation}</Label>
          <Label>{store.cuurentOwner?.accept_rate}</Label>
        </UserLabelView>
      </Info>
    )
  })
  return <ObsUserInfo/>
}

const styles = StyleSheet.create({
  image: {
    borderRadius:4,
    overflow:'hidden'
  }
})

export default UserInfo