import styled from "styled-components/native"
import { light } from "../../themes/style"
export const Container = styled.View`
    align-items: center;
    justify-content: flex-start;
    min-height: 300px;
    min-width: 80%;
    max-width: 80%;
    border-radius: 40px;
    overflow: hidden;
    align-self: center;
` 
export const Bar = styled.View`
    min-width: 100%;
    height: 60px;
    background: ${(p: typeof light) => p.theme.paper};
    flex-direction: row-reverse;
    align-items: center;
    justify-content: center;
`
export const QuestionMode = styled.View`
    min-width: 60%;
    max-width: 60%;
    flex-direction: row;
    padding-left: 10px;
    padding-right: 10px;
`
export const QuestionModeTab = styled.View`
    flex-grow: 1;
    align-items: center;
    justify-content: center;
    border: 1px solid ${(p: typeof light) => p.theme.border};
    border-bottom-width: 4px;
    background: ${(p: {isSelected: string}) => p.isSelected ? p.isSelected : 'transparent'};
`
export const QuestionModeTabLabel = styled.Text`
    color: ${(p: typeof light) => p.theme.text};
    ${(p: {isSelected: string}) => p.isSelected ? `color: ${p.isSelected}` : ``};
`
export const RemoveTextCircle = styled.View`
    align-items: center;
    justify-content: center;
    position: absolute;
    right: 12%;
    padding: 2px;
`
export const Info = styled.View`
    flex: 1;
    max-width: 100%;
    flex-direction: row-reverse;
    align-items: center;
    justify-content: center;
`
export const UserImageView = styled.View`
    flex: 1;
    max-width: 50%;
    align-items: center;
    justify-content: center;
`
export const UserImage = styled.View` 
    min-width: 60%;
    min-height: 60%;
    align-items: center;
    justify-content: center;
    border: 4px solid ${(p: typeof light) => p.theme.border};
    border-radius: 10px;
`
export const UserLabelView = styled.View`
    align-items: center;
    justify-content: space-between;
    flex: 1;
    max-width: 50%;
    max-height: 60%;
    min-height: 60%;
`
export const Label = styled.Text`
    color: ${(p: typeof light) => p.theme.text};
    font-size: 18px;
    text-align: right;
    min-width: 100%;
`