import React from "react"
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { useTheme } from "../themes"
import { LoadingIndicatorView } from "../themes/style"
import { ActivityIndicator } from "react-native"

const LoadingIndicator = () =>{
    const theme = useTheme()
    return(
        <LoadingIndicatorView>
            <ActivityIndicator color={theme.mode === 'light' ? '#000' : '#fff'} size="large" />
        </LoadingIndicatorView>
    )
}
export default LoadingIndicator