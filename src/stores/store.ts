import { runInAction } from "mobx";

interface Owner{
    accept_rate: number
    display_name: string
    link: string
    profile_image: string
    reputation: number
    user_id: number
    user_type: string
}
export interface StoreProps {   
    setSearchFilter: (userid: string) => Promise<boolean>
    responseMsg: string
    isRequestRunning: boolean
    actionRequestRunning(): void
    setResponseMsg(msg: string): void
    searchResult: []
    posts: []
    clearPost(): void
    isCurrentPostModalOpen: boolean
    actionCurrentPostModalOpen(): void
    cuurentOwner: Owner | null
    currentPost: {}
    setCurrentPost(post):void
    sortPostByField(field: string):void
    sortedPosts: []
}

export function store():StoreProps { 
    return {
        searchResult: [],
        posts: [],
        sortedPosts: [],
        currentPost: {},
        cuurentOwner: null,
        responseMsg: '',
        isCurrentPostModalOpen: false,
        isRequestRunning: false,
        actionRequestRunning(){
            runInAction(()=>{
                this.isRequestRunning = !this.isRequestRunning
            }) 
        },
        sortPostByField(field){
            let array = this.posts
            let key = field === 'Dates' ? 'creation_date' : field === 'Views' ? 'view_count' : 'answer_count' 
            this.actionRequestRunning()
            array.sort((a,b)=>{
                const itema = a[key];
                const itemb = b[key];
                console.debug('parseInt(a[key])',parseInt(a[key]))
                return parseInt(itema) - parseInt(itemb)
            })
            this.posts = []
            setTimeout(() => {
                this.actionRequestRunning()
                runInAction(()=>{
                        this.posts = array; 
                }) 
            }, 0);
        },
        setCurrentPost(post){
            runInAction(()=>{
                this.currentPost = post
            }) 
        },
        actionCurrentPostModalOpen(){
            runInAction(()=>{
                this.isCurrentPostModalOpen = !this.isCurrentPostModalOpen
            }) 
        },
        clearPost(){
            runInAction(()=>{
                this.posts = []
                this.cuurentOwner = null
                this.currentPost = {}
            })  
        },
        setResponseMsg(msg){
            runInAction(()=>{
                this.responseMsg = msg
            })
        },
        async setSearchFilter(userid){
            try{
                this.clearPost()
                this.setResponseMsg('')
                this.actionRequestRunning()
                console.debug('setSearchFilter')
                let headers = new Headers(); 
                let ops = {
                    method: 'GET',
                    headers: headers
                }
                const response = await fetch(`https://api.stackexchange.com/2.2/users/${userid}/questions?order=desc&sort=activity&site=stackoverflow`,ops);
                if(response){
                    let res = await response.json()
                    if(res['error_id'])
                        this.setResponseMsg(res['error_message'])
                    else{
                        runInAction(()=>{
                            console.debug(res['items'][0])
                            if(res['items'][0])
                                this.cuurentOwner = res['items'][0]['owner']
                            else
                                this.cuurentOwner = null
                            this.posts = res['items'];
                        })
                    }
                    return true
                }
                return false
            }catch(e){
                console.debug('setSearchFilter',e)
                this.actionRequestRunning()
                return false
            }finally{
                this.actionRequestRunning()
            }
        }
    }
}
  
export type TStore = ReturnType<typeof store>