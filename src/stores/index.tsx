import React from 'react'
import { store, TStore } from './store'
import { useLocalObservable } from 'mobx-react'

const StoreContext = React.createContext<TStore | null>(null)

export const StoreProvider = ({ children }: {children: JSX.Element}) => {
  const st = useLocalObservable(store)
  return <StoreContext.Provider value={st}>{children}</StoreContext.Provider>
}

export const useStore = () => {
  const st = React.useContext(StoreContext)
  if (!st) {
    throw new Error('useStore must be used within a StoreProvider.')
  }
  return st
}