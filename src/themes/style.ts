import styled from "styled-components/native"
export const light = {
    theme: {
      background: '#ededed',
      paper: '#e6e6e6',
      border: '#000',
      text: '#696969'
    }
}
export const dark = {
    theme: {
      background: '#202124',
      paper: '#303134',
      border: '#fff',
      text: '#ececec'
    }
}
export const Container = styled.View`
    margin: 0;
    padding: 5px;
    background: ${(p: typeof light) => p.theme.background};
    padding-top: ${(p: {paddingTop: number}) => p.paddingTop}px;
    flex: 1;
    height: 100%;
    align-items: flex-start;
    justify-content: flex-start;
    min-height: ${(p: {minHeight: number}) => p.minHeight}px;
`
export const Text = styled.Text`
    color: ${(p: typeof light) => p.theme.text};
`
export const Title = styled.Text`
    color: ${(p: typeof light) => p.theme.text};
    font-size: 26px;
    text-align: center;
    min-width: 100%;
`
export const NoDataView = styled.View`
    flex: 1;
    align-items: center;
    justify-content: center;
    min-height: 40%;
`
export const LoadingIndicatorView = styled.View`
    flex: 1;
    align-items: center;
    justify-content: center;
    min-height: 40%;
`
export const CurrentPostModalView = styled.View`
    flex: 1;
    align-items: center;
    justify-content: center;
    background: #0000004f;
`
export const BackCloseModal = styled.View`
    min-width: ${(p: {width: number}) => p.width}px;
    min-height: ${(p: {height: number}) => p.height}px;
    position: absolute;
    top: 0;
    z-index: -1;
`
export const CurrentPostModalViewInside = styled.View`
    max-width: 70%;
    min-height: 50%;
    background: ${(p: typeof light) => p.theme.background};
    border-radius: 4px;
    align-items: center;
    justify-content: center;
`
