import React, { createContext, useState, useEffect } from 'react'
import { StatusBar } from 'expo-status-bar'
import { ThemeProvider } from 'styled-components/native'
import { Appearance, AppearanceProvider } from 'react-native-appearance'

import {light} from './style'
import {dark} from './style'

const defaultMode = Appearance.getColorScheme() || 'light'

const ThemeContext = createContext({
    mode: defaultMode,
    setMode: (mode: React.SetStateAction<"light" | "dark" | "no-preference">) => console.log(mode)
})

export const useTheme = () => React.useContext(ThemeContext)

const ManageThemeProvider = ({ children }:{children: JSX.Element}) => {
    const [themeState, setThemeState] = useState(defaultMode)
    const setMode = (mode: React.SetStateAction<"light" | "dark" | "no-preference">) => {
      setThemeState(mode)
    }
    useEffect(() => {
      const subscription = Appearance.addChangeListener(({ colorScheme }: any) => {
        setThemeState(colorScheme)
      })
      return () => subscription.remove()
    }, [])
    return (
      <ThemeContext.Provider value={{ mode: themeState, setMode }}>
        <ThemeProvider
          theme={themeState === 'dark' ? dark.theme : light.theme}>
          <>
            <StatusBar
              style={themeState !== 'dark' ? 'dark' : 'light'}
            />
            {children}
          </>
        </ThemeProvider>
      </ThemeContext.Provider>
    )
}

const ThemeManager = ({ children }:{children: JSX.Element}) => (
    <AppearanceProvider>
        <ManageThemeProvider>{children}</ManageThemeProvider>
    </AppearanceProvider>
)
  
export default ThemeManager